# wetrace

deployment of wetrace services. follwoing figure discussed the architecture 
of the wetrace platfrom. 

![Alt text](wetrace-architecture.png?raw=true "wetrace platfrom architecture")

## services

three main servies

```
1. elassandra
2. aplos
3. gateway
```

## configuration

change `host.docker.local` field in `.env` file to local machines ip. also
its possible to add a host entry to `/etc/hosts` file by overriding
`host.docker.local` with local machines ip. following is an example of
`/etc/hosts` file.

```
10.4.1.104    host.docker.local
```

give write permission to `/private/var/services/connect/elassandra` directory
in the server. following is the way to give the permission

```
sudo mkdir /private/var/services/connect/elassandra
sudo chmo -R 777 /private
```

## deploy services

Start services in following order

```
docker-compose up -d elassandra
docker-compose up -d aplos
docker-compose up -d gateway
```

## connect apis

gateway service will start a REST api on `7654` port. for an example if your
machines ip is `10.4.1.104` the apis can be access via `10.4.1.104:7654/api/<apiname>`.
following are the available rest api end points and their specifications

#### 1. create vaccine

```
# request
curl -XPOST "http://localhost:7654/api/vaccines" \
--header "Content-Type: application/json" \
--header "Bearer: eyJkaWdzaWciOiJORWpYZnIwQjJMZG4ySGxPb2t5blp0dkNzSFVqMGFoVTVZd1F5TmJSVCtOYjlwTnBXcEsvUi9UbDZpanhPVVJiVlJHc2NHaFIrcWVCbkZhK09YYjBmMGlacVh0WHBDVXV6bnJOcTFKRmpGZC8zSU80L1o4SXl3WG1EdWFGcUg5Njc5VE9neVRkcU1nT01VeWNNWTF0bmtIUStWVUtUN0JTV0NWMEM3ZmNXbEE9IiwiaWQiOiJlcmFuZ2FlYkBnbWFpbC5jb20iLCJpc3N1ZVRpbWUiOjE1NTg0ODk4ODksInJvbGVzIjoiIiwidHRsIjo2MH0=" \
--data '
{
  "id": "111110",
  "execer": "admin:admin",
  "messageType": "addVaccine",
  "userMobile": "0715422017",
  "userName": "test user",
  "vaccineStatus": "done"
}
'

# reply
{"code":201,"msg":"vaccine added"}
```


#### 2. get vaccine 

```
# request
curl -XPOST "http://localhost:7654/api/vaccines" \
--header "Content-Type: application/json" \
--header "Bearer: eyJkaWdzaWciOiJORWpYZnIwQjJMZG4ySGxPb2t5blp0dkNzSFVqMGFoVTVZd1F5TmJSVCtOYjlwTnBXcEsvUi9UbDZpanhPVVJiVlJHc2NHaFIrcWVCbkZhK09YYjBmMGlacVh0WHBDVXV6bnJOcTFKRmpGZC8zSU80L1o4SXl3WG1EdWFGcUg5Njc5VE9neVRkcU1nT01VeWNNWTF0bmtIUStWVUtUN0JTV0NWMEM3ZmNXbEE9IiwiaWQiOiJlcmFuZ2FlYkBnbWFpbC5jb20iLCJpc3N1ZVRpbWUiOjE1NTg0ODk4ODksInJvbGVzIjoiIiwidHRsIjo2MH0=" \
--data '
{
  "id": "1111xx",
  "execer": "admin:admin",
  "messageType": "getVaccine",
  "userMobile": "0715422017"
}
'

# reply
{"userMobile":"0775321290","userName":"test user","vaccineStatus": "done", "timestamp":"2021-07-12 20:55:21.308"}
```


#### 3. search vaccines 

```
# request
curl -XPOST "http://localhost:7654/api/vaccines" \
--header "Content-Type: application/json" \
--header "Bearer: eyJkaWdzaWciOiJORWpYZnIwQjJMZG4ySGxPb2t5blp0dkNzSFVqMGFoVTVZd1F5TmJSVCtOYjlwTnBXcEsvUi9UbDZpanhPVVJiVlJHc2NHaFIrcWVCbkZhK09YYjBmMGlacVh0WHBDVXV6bnJOcTFKRmpGZC8zSU80L1o4SXl3WG1EdWFGcUg5Njc5VE9neVRkcU1nT01VeWNNWTF0bmtIUStWVUtUN0JTV0NWMEM3ZmNXbEE9IiwiaWQiOiJlcmFuZ2FlYkBnbWFpbC5jb20iLCJpc3N1ZVRpbWUiOjE1NTg0ODk4ODksInJvbGVzIjoiIiwidHRsIjo2MH0=" \
--data '
{
  "id": "1111xx",
  "execer": "admin:admin",
  "messageType": "searchVaccine",
  "offset": "0",
  "limit": "10",
  "userMobile": "",
  "userMobileTerm": "",
  "sort": "descending"
}
'

# reply
{"meta":{"count":"2","limit":"10","offset":"0","total":"2"},"vaccines":[{"timestamp":"2021-10-23T20:16:06.698Z","userMobile":"0715422019","userName":"ops user","vaccineStatus":"done"},{"timestamp":"2021-10-23T02:43:20.761Z","userMobile":"0715422017","userName":"test user","vaccineStatus":"done"}]}
```
